class SurveysController < ApplicationController
	before_filter :log_in_user, except: :show
	before_filter :create_survey, only: :new

  def index
    
  end
  
  def new
  	@survey = current_user.survey
  	@items = current_user.survey.items
  end

  def update  	
  	@survey = Survey.find(survey_params[:id])
  	@survey.build(survey_params[:temp_answers].split(','))
  	redirect_to @survey
  end

  def show
  	@survey = Survey.find(params[:format])
  end

  private
  def survey_params  	
    params.require(:survey).permit(:temp_answers, :id)
  end

  def log_in_user
  	#reset_session
  	render 'sessions/index' unless current_user.present?
  end

  def create_survey  	
    current_user.survey.present? && current_user.survey.done? ? redirect_to(current_user.survey) : Survey.destroy_and_create(current_user) 
  end
end
