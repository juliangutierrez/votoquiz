class Survey < ActiveRecord::Base
	has_one :user
	has_many :items, :dependent => :destroy
	belongs_to :candidate

	attr_accessor :temp_answers

	def self.done
		where(done: true)
	end

	def self.destroy_and_create user
		user.survey.destroy if user.survey.present?
		create(items: Item.build, user: user)
	end

	def self.candidate_pct candidate_id
		where(candidate_id: candidate_id).done.size / done.size.to_f
	end

	def self.unsure_pct
		where(candidate_id: nil).done.size / done.size.to_f
	end

	def correct_answers
		self.items.map { |item| item.proposal.candidate.id }
	end

	def answers
		self.items.map { |item| item.try(:candidate).try(:id) }
	end

	def results
		results_array = []
		correct_answers.each_with_index do |correct_answer, index|
			correct_answer == answers[index] ? results_array.push(true) : results_array.push(false)			
		end
		results_array
	end

	def accuracy
		results.count(true) / results.size.to_f
	end

	def build temp_answers
		update(candidate_id: temp_answers[0], done: true)
		items.each_with_index do |item, index|
			item.candidate_id = temp_answers[index + 1]
			item.save
		end
		#Result.update(self)
		self	
	end

	def done?
		done
	end
end
