class Result < ActiveRecord::Base
	serialize :correct_answers, Hash
	serialize :correct_answers_by_electors, Hash
	serialize :correct_answers_by_electors_total, Hash
	serialize :done_surveys, Hash

	def self.update survey
		new_result = Result.last.dup
		survey.items.each do |item|
			author_id = item.proposal.candidate_id
			if author_id == item.candidate_id
				new_result.correct_answers[author_id] += 1				 
				new_result.correct_answers_by_electors_total[survey.candidate_id] += 1
				new_result.correct_answers_by_electors[survey.candidate_id][author_id] += 1
			end
		end		
		new_result.done_surveys[survey.candidate_id] += 1
		new_result.save
	end
end
