class Item < ActiveRecord::Base
	belongs_to :survey
	belongs_to :proposal
	belongs_to :candidate
	
	def self.done		
		joins(:survey).where(:surveys=> {:done => true})
	end

	def self.build
		candidates = Candidate.all.shuffle
		candidates.map do |candidate|
			Item.new(proposal: candidate.proposals.sample)
		end
	end

	def correct?
		self.candidate.try(:id) == proposal.candidate.id
	end

	def skipped?
		candidate.nil?
	end
end
