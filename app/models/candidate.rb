class Candidate < ActiveRecord::Base
	has_many :proposals
	has_many :items, through: :proposals
	has_many :surveys

	def correct_answers	
		Item.joins(:proposal).where('items.candidate_id = ? AND items.candidate_id = proposals.candidate_id', id).size
	end

	def correct_answers_pct_total
		self.correct_answers / Item.joins(:proposal).where('items.candidate_id = proposals.candidate_id').size.to_f
	end

	def correct_answers_by_electors_pct_total
		surveys.present? ? Item.joins(:survey, :proposal).where('surveys.candidate_id = ? AND surveys.candidate_id = proposals.candidate_id', id).size / Item.joins(:proposal).where('items.candidate_id = proposals.candidate_id').size.to_f : 0
	end

	def self.correct_answers_by_unsure_electors_pct_total
		Candidate.items_by_surveys(nil).select {|item| item.correct? }.size / (Survey.where(candidate_id: nil).done.size * 11.0) 
	end

	def self.skipped_answers		
		Item.done.select { |item| item.skipped? }.size
	end

	def self.skipped_answers_pct
		skipped_answers / (Survey.done.size * 11.0)
	end

	def self.correct_answers_by_electors candidate_id, other_candidate_id
		#require 'pry'; binding.pry 		
		#items_by_electors(candidate_id, other_candidate_id).select { |item| item.correct? }.size
	end

	def self.correct_answers_pct_by_electors candidate_id, other_candidate_id
		Item.joins(:survey, :proposal).where('surveys.candidate_id = ? AND proposals.candidate_id = ? AND items.candidate_id = proposals.candidate_id', other_candidate_id, candidate_id).size	/ Item.joins(:survey, :proposal).where('surveys.candidate_id = ? AND surveys.candidate_id = proposals.candidate_id', candidate_id).size.to_f
	end

	def self.items_by_electors candidate_id, other_candidate_id
		Item.joins(:survey).where(:surveys=> {:candidate_id => candidate_id}).joins(:proposal).where(:proposals => {:candidate_id => other_candidate_id}).done
	end

	def self.skipped_answers_pct_by_electors candidate_id
		skipped_answers_by_electors(candidate_id) / items_by_surveys(candidate_id).size.to_f
	end

	def self.skipped_answers_by_electors candidate_id		
		items_by_surveys(candidate_id).select { |item| item.skipped? }.size
	end

	def self.skipped_answers_by_unsure
		items_by_surveys(nil).select { |item| item.skipped? }.size
	end
	
	def self.skipped_answers_pct_by_unsure
		skipped_answers_by_unsure / items_by_surveys(nil).size.to_f
	end

	def self.items_by_surveys candidate_id
		Item.joins(:survey).where(:surveys=> {:candidate_id => candidate_id}).done
	end

end
