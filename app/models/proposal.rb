class Proposal < ActiveRecord::Base
	belongs_to :candidate
	has_many :items
end
