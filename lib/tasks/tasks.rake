desc "Create surveys"
task :create_surveys do
  (0..100).each do |index|
		answers = [Candidate.all.map(&:id).sample] + Candidate.all.map(&:id).shuffle
		proposals =  Candidate.all.map(&:id).map do |id| Candidate.find(id).proposals.sample end
		items = proposals.map do |proposal| Item.new(proposal: proposal) end
		survey = Survey.new(items: items).build(answers)

		User.create(name: "user ##{index}", survey: survey)
	end
end