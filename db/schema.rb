# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180727012357) do

  create_table "candidates", force: true do |t|
    t.string   "name"
    t.string   "party"
    t.integer  "number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: true do |t|
    t.integer  "proposal_id"
    t.integer  "survey_id"
    t.integer  "candidate_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "items", ["survey_id"], name: "index_items_on_survey_id", using: :btree

  create_table "proposals", force: true do |t|
    t.text     "text"
    t.integer  "candidate_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "topic"
  end

  create_table "results", force: true do |t|
    t.text     "correct_answers"
    t.text     "correct_answers_by_electors"
    t.text     "correct_answers_by_electors_total"
    t.text     "done_surveys"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "surveys", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "candidate_id"
    t.string   "done"
  end

  add_index "surveys", ["candidate_id"], name: "index_surveys_on_candidate_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "survey_id"
  end

end
