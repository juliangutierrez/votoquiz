class AddDoneToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :done, :string
  end
end
