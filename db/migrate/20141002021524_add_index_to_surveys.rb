class AddIndexToSurveys < ActiveRecord::Migration
  def change
    add_index :surveys, :candidate_id
  end
end
