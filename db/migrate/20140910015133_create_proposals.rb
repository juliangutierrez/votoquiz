class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.text :text
      t.references :candidate
      
      t.timestamps
    end
  end
end
