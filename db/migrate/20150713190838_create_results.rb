class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
    	t.text :correct_answers
    	t.text :correct_answers_by_electors
    	t.text :correct_answers_by_electors_total
      t.text :done_surveys
      t.timestamps
    end
  end
end
