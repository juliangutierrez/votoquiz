class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|      
			t.references :proposal
			t.references :survey
			t.references :candidate

      t.timestamps
    end
  end
end
